# -*- mode: python; tab-width: 4; indent-tabs-mode: nil -*-
from guardian import GuardState, NodeManager
import math
import numpy as np
import time
import cdsutils

import isclib.matrices as matrix

from isclib.epics_average import EzAvg, ezavg

##################################################
# Variables

#Tolerance for rerunning
EPICS_TOLERANCE = 0.14

# threshold to recognize when there is no light on the refl PD
imc_refl_no_light_threshold = 10

# gain in db for IMC servo during acquisition
imc_servo_acquire_in1gain = -13#-26

#Input to waveplate threshold
imc_input_power_threshold = 2

#imc_servo_1W_in1gain = 13  #for O3 VCO
#imc_servo_1W_in1gain = 12   #for LHO VCO Synergy AJM210106
imc_servo_1W_in1gain = 10 


# imc fast gain (This used to be 3)
#imc_servo_1W_fastgain = 7  #for O3 VCO
imc_servo_1W_fastgain = 4   #for LHO VCO Synergy AJM210106

# The PSL common gain (dB) set at TransPD value of 4V
psl_fss_commgain_at_4V = 16 #17 lowered by 1dB #   (was 14 with previous TTFSS)


##################################################
# Functions

def psl_fault():
    #JCB 2016/12/5 added check on PSL guardian
    #Check to see if the PSL is in a FAULT condition
    if nodes['PSL'].state == 'FAULT':
        notify("PSL guardian in fault state")
        return True
    return False

# check for some error conditions
def in_fault():
    # currently these faults are only checked in acquire and in the
    # fault state, if the MC is not already locked.  This is intended
    # to help a user diagnose problems if the MC is not locking, but
    # not to interefere with the MC lock needlessly by taking it out
    # of the locked state when it is still locked
    message = []
    flag = False

    # a hierarchy of conditions to check if there is light on the refl
    # PD (only give user most relevant fault message)
    if ezca['PSL-PMC_LOCK_ON'] != 1:    #-30000:
        message.append("PMC unlocked")
        flag = True
    #elif ezca['PSL-PERISCOPE_A_DC_ERROR_FLAG'] != 1:
    elif ezca['IMC-PWR_IN_OUTPUT'] < (ezca['PSL-POWER_REQUEST']-2):
        message.append("PSL periscope PD error (low light?)")
        flag = True
    elif ezca['IMC-REFL_DC_OUTPUT'] < imc_refl_no_light_threshold:
        message.append("IMC REFL PD no light (check PZT/MC1 alignment)")
        flag = True
    elif ezca['IMC-PWR_EOM_OUTPUT'] < imc_input_power_threshold:
        message.append("Input power before waveplate below 2 watts, please check")
        flag = True

    # check if FSS is locked            #Refcav bypass, comment out AJM20190205
    if ezca['PSL-FSS_AUTOLOCK_STATE'] != 4:
        message.append("FSS unlocked")
        if psl_eom_ringing():
            toggle_ttfss_autolocker()
        flag = True

    # check HAM 2/3 ISI watchdogs
    # FIXME: this should look at ISI guardian nodes
    if ezca['ISI-HAM2_WD_MON_STATE_INMON'] != 1:
        message.append("HAM2 ISI tripped")
        flag = True
    if ezca['ISI-HAM3_WD_MON_STATE_INMON'] != 1:
        message.append("HAM3 ISI tripped")
        flag = True

    # check for MC sus guardian alignment states
    for sus in ['MC1', 'MC2', 'MC3']:
        if nodes_loose['SUS_'+sus] != 'ALIGNED':
            message.append(sus+" not aligned")
            flag = True

    # if we get a flag, notify the user, otherwise clear any
    # notifications
    if flag:
        notify(', '.join(message))
    else:
        notify()

    return flag

# check for lock
def is_locked():
    # if the front-end lock trigger is 1 then we're locked
    #return ezca['IMC-MCL_FM_TRIG_MON'] == 1
    # FIXME: we have to look at the trans sum directly, since the
    # trigger is actually shut off after lock is acquired.  Is that
    # necessary?  If not, we can just continue to look at the trigger
    # state (above)
    return ezca['IMC-MC2_TRANS_SUM_INMON'] >= 50*ezca['IMC-PWR_IN_OUTPUT']
    #return ezca['IMC-MC2_TRANS_SUM_INMON'] >= 40*ezca['IMC-PWR_IN_OUTPUT']


def diff_fss_gain():

    #refcav_trans = cdsutils.avg(-10,'PSL-FSS_TPD_DC_OUTPUT')
    refcav_trans = ezavg(ezca,5,'PSL-FSS_TPD_DC_OUTPUT')

    if refcav_trans < 1.0:
        log("Reference Cavity power is too low, do not step")
        d_fss_gain = 0

    else:
        fss_gain_set = psl_fss_commgain_at_4V+20*math.log(4.0/refcav_trans,10)	#calculates what gain we want
        fss_gain_current = ezca['PSL-FSS_COMMON_GAIN']		#the current gain
        d_fss_gain = round(fss_gain_set - fss_gain_current,1)	#what change we need to make to 1 decimal place

    return d_fss_gain


def psl_eom_ringing():

    if abs(ezca['PSL-FSS_PC_MON_OUTPUT']) > 100:
        notify('PSL EOM has oscillation')
        return True
    else:
        return

def toggle_ttfss_autolocker():
    log('Toggling the ttfss autolocker')
    ezca['PSL-FSS_AUTOLOCK_ON'] = 0
    time.sleep(1)
    ezca['PSL-FSS_AUTOLOCK_ON'] = 1
    time.sleep(1)

#################################################
# DECORATORS
#################################################

#class assert_psl_ok(GuardStateDecorator):
#    def pre_exec(self):
#        if psl_fault():
#            return 'PSL_FAULT'

##################################################

#nodes = NodeManager(['SUS_MC1','SUS_MC2','SUS_MC3','PSL'])
#MANAGED_NODES = ['PSL']
nodes = NodeManager(['PSL'])
nodes_loose = NodeManager(['SUS_MC1','SUS_MC2','SUS_MC3'])
##################################################

# initial request on initialization
#request = 'LOCKED'
request = 'DOWN'
# nominal operating state
#nominal = 'FINAL_IFO_POWER'
nominal = 'PSL_TRACKING'

##############################################
# determine lock state on initialization
class INIT(GuardState):
    request = True

    def main(self):

        #nodes.set_managed(MANAGED_NODES)
        nodes.set_managed()
# CA Commented out this section 180604 for troubleshooting. 
#        if is_locked():
#            if nodes['PSL'] == 'POWER_RESET':
#                return 'LOCKED_1W'
#            elif nodes['PSL'] == 'POWER_1W_NO_REFL_SCALE':
#                return 'LOCKED_1W'
#            elif nodes['PSL'] == 'POWER_4W_NO_REFL_SCALE':
#                return 'LOCKED_4W'
#            elif nodes['PSL'] == 'POWER_10W':
#                return 'LOCKED_10W'
#            elif nodes['PSL'] == 'POWER_20W':
#                return 'LOCKED_20W'
#            elif nodes['PSL'] == 'POWER_25W':
#                return 'LOCKED_25W'
#            elif nodes['PSL'] == 'POWER_45W':
#                return 'LOCKED_45W'
#            elif nodes['PSL'] == 'IFO_POWER':
#                return 'IFO_POWER'
#            else:
#                return 'LOCKED'
#        else:
#            return 'DOWN'

##############################################
class PSL_FAULT(GuardState):
    index = 5
    redirect = False
    request = False

    def main(self):
        #nodes.set_managed(MANAGED_NODES)
        nodes.set_managed()
        nodes['PSL'] = 'POWER_RESET'

    def run(self):
        if psl_fault():
            return
        return 'DOWN'

##############################################
# wait for all faults to be cleared, the jump to init to find state
class FAULT(GuardState):
    index = 10
    redirect = False
    request = False
    
    def main(self):
        #nodes.set_managed(MANAGED_NODES)
        nodes.set_managed()
        if ezca['IMC-PWR_EOM_OUTPUT'] < imc_input_power_threshold:
            nodes['PSL'] = 'POWER_SAFE'
        else:
            nodes['PSL'] = 'POWER_RESET'

        if ezca['SYS-MOTION_C_SHUTTER_A_STATE']:
            log('IMC REFL shutter is closed')
            if ezca['IMC-PWR_IN_OUTPUT'] < 20:
                log('Opening IMC REFL shutter now')
                ezca['SYS-MOTION_C_SHUTTER_A_OPEN'] = 1
            else:
                log('Input power is high, should lower.')

    def run(self):
        
        if in_fault():
            if ezca['SYS-MOTION_C_SHUTTER_A_STATE']:
                log('IMC REFL shutter is closed')
                if ezca['IMC-PWR_IN_OUTPUT'] < 20:
                    log('Opening IMC REFL shutter now')
                    ezca['SYS-MOTION_C_SHUTTER_A_OPEN'] = 1
                else:
                    log('Input power is high, should lower.')
            return
        return 'INIT'

##############################################
#JCB 2016/12/5  Added 0W state for requesting stuff no power from PSL
class GOTO_UNLOCKED_0W(GuardState):
    index = 15
    request = False

    def main(self):
        nodes['PSL'] = 'POWER_SAFE'

    def run(self):
        return nodes['PSL'].arrived
        #return True

class UNLOCKED_0W(GuardState):
    index=16
    request = True

    def run(self):
        return True

##############################################
# reset everything
class DOWN(GuardState):
    index = 20
    goto = True

    def main(self):

        # self.timer['conf'] = 5
        #nodes.set_managed(MANAGED_NODES)
        nodes.set_managed()
        nodes['PSL'] = 'POWER_RESET'

        #JCB Make sure the MCL filter history is not at infinity and passing bad data
        ezca['LSC-MCL_RSET'] = 2

        # request suspensions aligned
        for sus in ['MC1','MC2','MC3']:
            #nodes['SUS_'+sus] = 'ALIGNED'
            nodes_loose['SUS_'+sus] = 'ALIGNED'

        # # turn off the ISS second loop and reacquire at the current power level later in this script
        #ezca['PSL-ISS_SECONDLOOP_CLOSED'] = 0
        #ezca['PSL-ISS_SECONDLOOP_GAIN'] = 40

        # FIXME: this is redundant with some of below
        ezca['IMC-L_GAIN'] = 0
        ezca['IMC-L_TRAMP'] = 0.1
        ezca['IMC-L_OFFSET'] = -1548
        ezca['IMC-L_LIMIT'] = 0
        #ezca['IMC-L_SW1S'] = 28 #offset on with 12, off with 4
        #ezca['IMC-L_SW2S'] = 1536
        ezca.switch('IMC-L','INPUT','OFFSET','FM1','OUTPUT','ON')
        ezca.switch('IMC-MCL','INPUT','OUTPUT','ON')
        ezca['IMC-MCL_GAIN'] = -125 #No refcav bypass
        #ezca['IMC-MCL_GAIN'] = 0 #REFCAV bypass
        #ezca.switch('IMC-MCL','INPUT','OUTPUT','OFF') #refcav bypass
        
        #ezca['IMC-REFL_SERVO_FASTEN'] = 'Off'
        #ezca['IMC-MCL_MASK_FM2'] = 1
        #ezca.switch('IMC-MC2_TRANS_SUM', 'FM1', 'ON')

        #turn off psl temp loop with refcav bypass #AJM20190205
        #ezca['PSL-FSS_TEMP_LOOP_ON_REQUEST'] = 0
        #ezca['PSL-FSS_TEMP_LOOP_RSET'] = 2
        # turn off psl pzt loop
        #ezca['PSL-FSS_LOOP_MODE'] = 0


        # turn off feedback
        ezca['IMC-L_GAIN'] = 0      #1
        ezca['IMC-REFL_SERVO_FASTEN'] = 'Off'  #comment out for refcav bypass AJM20190205


        # turn off MC2 length feedback
        ezca['SUS-MC2_M1_LOCK_L_GAIN'] = 0
        ezca['SUS-MC2_M2_LOCK_L_TRAMP'] = 0
        ezca['SUS-MC2_M2_LOCK_L_GAIN'] = 0
        ezca.switch('SUS-MC2_M2_LOCK_L','FM3','OFF')
        ezca.switch('SUS-MC2_M1_LOCK_L','INPUT','OFF')
        # clear M1 length lock history
        ezca['SUS-MC2_M1_LOCK_L_RSET'] = 2
        ezca['SUS-MC2_M2_LOCK_L_RSET'] = 2
        ezca['SUS-MC2_M2_LOCK_L_TRAMP'] = 1


        # Make sure WFS whitening is only stage 1
        #system("perl /opt/rtcds/userapps/release/cds/common/scripts/beckhoff_gang_whiten

        # Turn off WFS_DC centering loops (DOF3)
        ezca.switch('IMC-DOF_3_P','INPUT','OFF')
        ezca.switch('IMC-DOF_3_Y','INPUT','OFF')

        # Turn off WFS loops and MC2-trans loop
        for dof1 in ['1','2','4']:
            for dof2 in ['P','Y']:
                ezca.switch('IMC-DOF_'+dof1+'_'+dof2,'INPUT','OFF')

        time.sleep(1)

        # turn off WFS and clear history
        # ezca['IMC-WFS_SWTCH'] = 'OFF'
        for dof1 in ['1','2','3','4']:
            for dof2 in ['P','Y']:
                ezca['IMC-DOF_'+dof1+'_'+dof2+'_RSET'] = 2

        # Turn off PZT resonance damping loops (DOF5)
        ezca.switch('IMC-DOF_5_P','INPUT','OFF')
        # ezca.switch('IMC-DOF_5_Y', 'INPUT', 'OFF') # not used, uncomment if needed

        # reset IMC "common mode" servo settings
        ezca['IMC-REFL_SERVO_COMCOMP'] = 'On'
        ezca['IMC-REFL_SERVO_COMFILTER'] = 'Off'
        ezca['IMC-REFL_SERVO_COMBOOST'] = 0
        ezca['IMC-REFL_SERVO_IN1POL'] = 0
        ezca['IMC-REFL_SERVO_FASTPOL'] = 0

        #Now controlled by the PSL guardian, reset in the power_reset state
        #ezca['IMC-REFL_SERVO_IN1GAIN'] = imc_servo_acquire_in1gain
        ezca['IMC-REFL_SERVO_FASTGAIN'] = 3 # 3 for O3 VCO
        # ezca['IMC-REFL_SERVO_FASTGAIN'] = -23 # accomodates extra gain of temp VCO (4 MHz/V) AE 05/2018
        ezca['IMC-REFL_SERVO_IN1EN'] = 'On'
        ezca['IMC-REFL_SERVO_IN2EN'] = 'Off'
        ezca['IMC-REFL_SERVO_SLOWBYPASS'] = 'On'

        # reset IMC MCL gain
        ezca['IMC-MCL_TRAMP'] = 0
        time.sleep(0.2)
        ezca['IMC-MCL_GAIN'] = -125 #no REFCAV bypass
        #ezca['IMC-MCL_GAIN'] = 0 # REFCAV bypass
        ezca.switch('IMC-MCL','FM3','FM4','FM10','ON','FM5','FM8','OFF') # no "REFCAV bypass"  filter settings
        #ezca.switch('IMC-MCL','FM3','FM4','FM8','FM10','OFF','FM5','ON') # "REFCAV bypass" filter settings
        time.sleep(0.1)
        ezca['IMC-MCL_RSET'] = 2

        self.timer['conf'] = 5
        

    def run(self):
        # wait for suspensions to become aligned
        for node in ['SUS_MC1','SUS_MC2','SUS_MC3']:
            #if nodes[node] != 'ALIGNED':
            #    if nodes[node].STALLED:
            #        nodes[node].revive()
            #    else: log('Check MC suspensions')
            #    return
            if nodes_loose[node] != 'ALIGNED':
                if nodes_loose[node].STALLED:
                    nodes_loose[node].revive()
                else: log('Check MC suspensions')
                return

        if psl_eom_ringing():
            toggle_ttfss_autolocker()
            return

        if psl_fault():
            return 'PSL_FAULT'
        if in_fault():
            return 'FAULT'
        #return True    #This line skips the timer, which I don't think we want to do - ajm20190905

        if not self.timer['conf']:
            return

        return True

##############################################
# turn on acquire feedback, and wait for lock
class ACQUIRE(GuardState):
    index = 40
    request = False

    def main(self):

        # Run down if waveplate hasn't been restored properly.
        if ezca["IMC-PWR_IN_OUTPUT"] < 0.8:
             notify('Input power is too small. Going to down.')
             return "DOWN"

        # Turn off M2 offset FIXME: Why this here and not down AJM190905
        ezca.switch('SUS-MC2_M2_LOCK_L','OFFSET','OFF')
        # engage feedback to servo and MC2
        ezca['IMC-REFL_SERVO_IN1EN'] = 'Off'       #comment out for refcav bypass
        ezca['IMC-REFL_SERVO_FASTEN'] = 1
        ezca.switch('IMC-L','INPUT','OUTPUT','ON')
        #ezca.switch('SUS-MC2_M2_LOCK_L','FM3','OUTPUT','ON')    #TODO do we want this on here, AE took out 190219
        #ezca['SUS-MC2_M2_LOCK_L_GAIN'] = 0.03                    #TODO do we want this on here, AE a little M2 190219 0.05 reduced CB 20190222
        ezca['SUS-MC2_M2_LOCK_OUTSW_L'] = 'ON'
        ezca.switch('SUS-MC2_M3_LOCK_L','OUTPUT','ON')
        ezca['SUS-MC2_M3_LOCK_OUTSW_L'] = 1  #'ON'
        #ezca['IMC-L_GAIN'] = 1     #AJM20191014 trigger test, 
        ezca['IMC-L_GAIN'] = 0
        ezca['IMC-REFL_SERVO_IN1EN'] = 'On'
        time.sleep(1)
        #ezca['PSL-FSS_LOOP_MODE'] = 2   #refcav bypass

        # engage slow path filters
        # ezca.switch('IMC-MCL', 'FM1', 'FM2', 'ON')

        # turn off trigger PD low-pass during lock
        # filter is empty anyways
        # ezca.switch('IMC-MC2_TRANS_SUM', 'FM1', 'OFF')

        # turn on filter triggering
        #ezca['IMC-MCL_MASK_FM2'] = 1
        self.timer['too_long_to_acquire'] = 20 #15 #30

    def run(self):
        if psl_fault():
            return 'PSL_FAULT'
        if in_fault():
            return 'FAULT'        

        log('waiting for lock...')
        if is_locked():
            return True
        
        if self.timer['too_long_to_acquire']:
            return 'ACQUIRE'

##############################################
class ENGAGE_MC2(GuardState):
    index = 45
    request = False

    def main(self):
        ezca['IMC-L_GAIN'] = 1
        self.timer['wait'] = 0.1
        
    def run(self):
        if psl_fault():
            return 'PSL_FAULT'
        if in_fault():
            return 'FAULT'
        if not self.timer['wait']:
            return
        if not is_locked():
            return 'ACQUIRE'
        return True

##############################################
# pause to settle and confirm we're in a somewhat
# stable lock
class LOCK_CONFIRM(GuardState):
    index = 50
    request = False

    def main(self):
        #time.sleep(2)
        self.timer['conf'] = 1
        #self.comm_gain_set = 7  #refcav bypass


    def run(self):
        # notify on faults
        if psl_fault():
            return 'PSL_FAULT'
        in_fault()
        
        # if lock not confirmed, drop back to ACQUIRE
        if not is_locked():
            return 'ACQUIRE'

        # refcav bypass
        #if ezca['IMC-REFL_SERVO_IN1GAIN'] < self.comm_gain_set: 
        #    ezca['IMC-REFL_SERVO_IN1GAIN'] += 1
        #    return

        if self.timer['conf']:
            return True


##############################################
# turn on boosts and ramp up servo input gain
class LOCK_UP(GuardState):
    index = 60
    request = False

    def main(self):

        ezca['IMC-MCL_TRAMP'] = 10
        ezca['IMC-MCL_RSET'] = 2
        #time.sleep(5)
        #ezca.switch('IMC-MCL','INPUT','OUTPUT','ON')    #TODO Think this should be on already
        time.sleep(1)
        #ezca['IMC-MCL_GAIN'] = -400 # REFCAV bypass, engage MCL with FAST_MON
        

        ## turn on m2 lock output and boost
        ezca['SUS-MC2_M2_LOCK_L_GAIN'] = 0.05 # changed from 0.1 after REFCAV bypass changes but keep at 0.5
        ezca.switch('SUS-MC2_M2_LOCK_L', 'FM3', 'ON')

        #ezca['IMC-REFL_SERVO_COMBOOST'] = 3   #refcav bypass AJM20190205

        # turn off filter triggering
        # FIXME: why do we turn thesee off once lock is acquired?
        #ezca['IMC-MCL_MASK_FM2'] = 'OFF'

        # turn back off trigger PD low-pass
        #ezca.switch('IMC-MC2_TRANS_SUM', 'FM1', 'ON')
        self.comm_gain_set = imc_servo_1W_in1gain #+ 20*math.log(1.0/ezca['IMC-PWR_IN_OUTPUT'],10)      #with refcav
        #self.comm_gain_set = 20 + 20*math.log(1.0/ezca['IMC-PWR_IN_OUTPUT'],10)      #with refcav
        #self.comm_gain_set = 7 + 20*math.log(1.0/ezca['IMC-PWR_IN_OUTPUT'],10)      #refcav bypass AJM20190205

        #time.sleep(5)
        #self.timer['temp_stable'] = 10


    def run(self):
        # notify on faults
        if psl_fault():
            return 'PSL_FAULT'
        in_fault()


        if not is_locked():
            log('lock loss, returning to down...')
            return 'DOWN'

        # ramp up the servo gain from its initial value to 0.
        # guardian runs at 32 Hz, so this should take 1 second.
        # complete state when it reaches desired gain
        # FIXME: this should be improved with a better built-in
        # guardian method
        if ezca['IMC-REFL_SERVO_IN1GAIN'] < self.comm_gain_set:
            ezca['IMC-REFL_SERVO_IN1GAIN'] += 1
            return

        #if self.timer['temp_stable']:
        #    return True

        #ezca['IMC-REFL_SERVO_IN1GAIN'] = self.comm_gain_set    # This shouldnt be necessary with above stepping AJM 20190205
        return True

##############################################
# turn on the last few filters and gains, then check for lock loss
class LOCK_UP2(GuardState):
    index = 70
    request = False

    def main(self):
        # engage the servo comboost
        ezca['IMC-REFL_SERVO_COMBOOST'] = 1  # with refcav

        # engage M1 path
        ezca['SUS-MC2_M1_LOCK_L_RSET'] = 2
        ezca.switch('SUS-MC2_M1_LOCK_L','FM2','FM1','INPUT','ON')
        ezca['SUS-MC2_M1_LOCK_L_TRAMP'] = 1
        ezca['SUS-MC2_M1_LOCK_L_GAIN'] = 1
        # timer to wait for gain ramp
        # self.timer['tramp'] = 3

        # dac offset on because glitches
        ezca.switch('SUS-MC2_M2_LOCK_L','OFFSET','ON')

        # Ensure IMC WFS Master Gain is 1
        ezca['IMC-WFS_GAIN'] = 1

        # Set IMC WFS Master to 0
        #ezca['IMC-WFS_GAIN'] = 0

        # Set WFS limits to 400
        for dof1 in ['1','2','3','4']:
            for dof2 in ['P','Y']:
                ezca['IMC-DOF_'+dof1+'_'+dof2+'_LIMIT'] = 400

        # Turn on the WFS limits
        for dof1 in ['1','2','3','4']:
            for dof2 in ['P','Y']:
                ezca.switch('IMC-DOF_'+dof1+'_'+dof2,'LIMIT','ON')
        time.sleep(2)


        # Turn on the WFS loops and MC2 trans loop
        for dof1 in ['1','2','4']:
            for dof2 in ['P','Y']:
                ezca.switch('IMC-DOF_'+dof1+'_'+dof2,'INPUT','ON')
            time.sleep(2)

        # Turn on the PZT resonance damping loop
        ezca.switch('IMC-DOF_5_P','INPUT','ON')
        # ezca.switch('IMC-DOF_5_Y', 'INPUT', 'ON')  # not used, uncomment if needed

        # These are the timer and flag pertaining to the wfs_dc loops 
        self.timer['wfs_dc'] = 10 #AE changed to 10 seconds

        #self.step = cdsutils.Step(ezca,'IMC-WFS_GAIN','+0.1,10',time_step=1)
        # turn on first boost
        ezca['IMC-REFL_SERVO_COMBOOST'] = 2

    def run(self):
        # notify on faults
        if psl_fault():
            return 'PSL_FAULT'
        in_fault()

        if not is_locked():
            log('lock loss, returning to down...')
            return 'DOWN'

        #return self.step.step()

        # wait for gain ramp before turning boost
        #if self.timer['tramp']:
        #ezca.switch('SUS-MC2_M1_LOCK_L', 'FM1', 'ON')

        # Waiting for 180s to allow all other loops to settle before 
        # turning on the DC spot centering:
        if not self.timer['wfs_dc']:
            return
        ezca.switch('IMC-DOF_3_P', 'INPUT', 'ON')
        ezca.switch('IMC-DOF_3_Y', 'INPUT', 'ON')

        #ezca.switch('IMC-MCL','FM8','ON') # REFCAV bypass (Why is this after WFS?)
        
        return True


##############################################
class LOCK_UP3(GuardState):
    index = 80
    request = False

    def main(self):
        # Set FSS gain based on refcav trans (13dB for 4V trans)
        #refcav_trans = ezavg(ezca,5,'PSL-FSS_TPD_DC_OUTPUT')
        
        #fss_gain_set = psl_fss_commgain_at_4V+20*math.log(4.0/refcav_trans,10)	#calculates what gain we want
        #fss_gain_current = ezca['PSL-FSS_COMMON_GAIN']		#the current gain
        #d_fss_gain = round(fss_gain_set - fss_gain_current,1)	#what change we need to make to 1 decimal place
        d_fss_gain = diff_fss_gain()
        #log('Expected gain' + str(fss_gain_set))

        if d_fss_gain < 0:
            fss_gStep = -0.1
        else:
            fss_gStep = +0.1

        self.fss_nStep = int(round(abs(d_fss_gain/fss_gStep)))
        fss_string = str(fss_gStep) + ',' + str(self.fss_nStep)
        log('Number of FSS steps is ' + str(self.fss_nStep))
        log('Step size is ' + str(fss_gStep))
        self.step_check = 0
        if not self.fss_nStep == 0:   #Don't change gain, if there is no need to change gain
            self.fss_step = cdsutils.Step(ezca,'PSL-FSS_COMMON_GAIN', fss_string, time_step=0.2)


        # Increase the IMC gain by 8dB by increasing the IMC servo fast gain and the digital IMC gain
        imcfg_current = ezca['IMC-REFL_SERVO_FASTGAIN']
        d_imcfg = int(imc_servo_1W_fastgain - imcfg_current)
        nSteps = str(d_imcfg)
        log("Number of IMC G steps is {}".format(nSteps))
        log("Fast Gain changing from {0} to {1}".format(imcfg_current,imc_servo_1W_fastgain))
        ezca['IMC-MCL_TRAMP'] = 1

        #self.imcf_step = cdsutils.Step(ezca,'IMC-REFL_SERVO_FASTGAIN', '+1.0,'+nSteps,time_step=1.0)
        #self.imcs_step = cdsutils.Step(ezca,'IMC-MCL_GAIN', '+1dB,'+nSteps,time_step=1.0)

        self.imcg_steps = [
            cdsutils.Step(ezca,'IMC-REFL_SERVO_FASTGAIN', '+1.0,'+nSteps,time_step=1.0),
            cdsutils.Step(ezca,'IMC-MCL_GAIN', '+1dB,'+nSteps,time_step=1.0),
            ]

    def run(self):

        #return True

        # notify on faults
        if psl_fault():
            return 'PSL_FAULT'
        in_fault()

        if not is_locked():
            log('lock loss, returning to down...')
            return 'DOWN'

        # FSS stepping, if needed
        if not self.fss_nStep == 0:
            if not self.fss_step.step():
                return

        # IMC stepping FIXME: This steps one, than the other.
#        if (not self.imcf_step.step()) or (not self.imcs_step.step()):
#            return

        # Replaced above with this code, which steps fast and slow together AJM20201015
        done = True
        for step in self.imcg_steps:
            done &= step.step()
        if not done:
            return


        return True

##############################################
class LOCKED(GuardState):
    index = 90
    requrest = True

    def run(self):
        # notify on faults
        if psl_fault():
            return 'PSL_FAULT'
        in_fault()

        if not is_locked():
            log('lock loss, returning to down...')
            return 'DOWN'

        return True

##############################################
class GOTO_LOCKED_1W(GuardState):
    index = 99
    request = False
    def main(self):
        nodes['PSL'] = 'POWER_1W_NO_REFL_SCALE'

    def run(self):
        # notify on faults
        if psl_fault():
            return 'PSL_FAULT'
        in_fault()

        if not is_locked():
            log('lock loss, returning to down...')
            return 'DOWN'

        return nodes['PSL'].arrived
        #return True

##############################################
class LOCKED_1W(GuardState):
    index=100
    request = True

    def run(self):
        if psl_fault():
            return 'PSL_FAULT'
        in_fault()
        if not is_locked():
            log('lock loss, returning to down...')
            return 'DOWN'
        return True

##############################################
class GOTO_LOCKED_4W(GuardState):
    index = 399
    request = False
    def main(self):
        nodes['PSL'] = 'POWER_4W_NO_REFL_SCALE'

    def run(self):
        #notify on faults
        if psl_fault():
            return 'PSL_FAULT'
        in_fault()

        if not is_locked():
            log('lock loss, returning to down...')
            return 'DOWN'

        return nodes['PSL'].arrived
        #return True

##############################################
class LOCKED_4W(GuardState):
    index=400
    request = True

    def run(self):
        if psl_fault():
            return 'PSL_FAULT'
        in_fault()
        if not is_locked():
            log('lock loss, returning to down...')
            return 'DOWN'
        return True

##############################################
class GOTO_LOCKED_10W(GuardState):
    index = 999
    request = False
    def main(self):
        nodes['PSL'] = 'POWER_10W'

    def run(self):
        #notify on faults
        if psl_fault():
            return 'PSL_FAULT'
        in_fault()

        if not is_locked():
            log('lock loss, returning to down...')
            return 'DOWN'

        return nodes['PSL'].arrived
        #return True

##############################################
class LOCKED_10W(GuardState):
    index=1000
    request = True

    def run(self):
        if psl_fault():
            return 'PSL_FAULT'
        in_fault()
        if not is_locked():
            log('lock loss, returning to down...')
            return 'DOWN'
        return True

##############################################
class GOTO_LOCKED_20W(GuardState):
    index = 1999
    request = False
    def main(self):
        nodes['PSL'] = 'POWER_20W'

    def run(self):
        #notify on faults
        if psl_fault():
            return 'PSL_FAULT'
        in_fault()

        if not is_locked():
            log('lock loss, returning to down...')
            return 'DOWN'

        return nodes['PSL'].arrived
        #return True

##############################################
class LOCKED_20W(GuardState):
    index=2000
    request = True

    def run(self):
        if psl_fault():
            return 'PSL_FAULT'
        in_fault()
        if not is_locked():
            log('lock loss, returning to down...')
            return 'DOWN'
        return True

##############################################
class GOTO_LOCKED_25W(GuardState):
    index = 2499
    request = False
    def main(self):
        nodes['PSL'] = 'POWER_25W'

    def run(self):
        #notify on faults
        if psl_fault():
            return 'PSL_FAULT'
        in_fault()

        if not is_locked():
            log('lock loss, returning to down...')
            return 'DOWN'

        return nodes['PSL'].arrived
        #return True

##############################################
class LOCKED_25W(GuardState):
    index=2500
    request = True

    def run(self):
        if psl_fault():
            return 'PSL_FAULT'
        in_fault()
        if not is_locked():
            log('lock loss, returning to down...')
            return 'DOWN'
        return True

##############################################
class GOTO_LOCKED_45W(GuardState):
    index = 4499
    request = False
    def main(self):
        nodes['PSL'] = 'POWER_45W'

    def run(self):
        #notify on faults
        if psl_fault():
            return 'PSL_FAULT'
        in_fault()

        if not is_locked():
            log('lock loss, returning to down...')
            return 'DOWN'

        return nodes['PSL'].arrived
        #return True

##############################################
class LOCKED_45W(GuardState):
    index=4500
    request = True

    def run(self):
        if psl_fault():
            return 'PSL_FAULT'
        in_fault()
        if not is_locked():
            log('lock loss, returning to down...')
            return 'DOWN'
        return True

##############################################
class IFO_POWER(GuardState):
    index=9999
    request = True

    def main(self):
        nodes['PSL'] = 'GOTO_POWER_EPICS'

    def run(self):
        #notify on faults
        if psl_fault():
            return 'PSL_FAULT'
        in_fault()

        if not is_locked():
            log('lock loss, returning to down...')
            return 'DOWN'

        if nodes['PSL'].arrived and nodes['PSL'] == 'GOTO_POWER_EPICS':
            nodes['PSL'] = 'POWER_EPICS'
        elif nodes['PSL'].arrived and nodes['PSL'] == 'POWER_EPICS':
            return True

        #return nodes['PSL'].arrived
        #return True

##############################################
class FINAL_IFO_POWER(GuardState):
    index=99999
    request = True

    def main(self):
        nodes['PSL'] = 'GOTO_FINAL_POWER_EPICS'

    def run(self):
        #notify on faults
        if psl_fault():
            return 'PSL_FAULT'
        in_fault()

        if not is_locked():
            log('lock loss, returning to down...')
            return 'DOWN'

        if nodes['PSL'].arrived and nodes['PSL'] == 'GOTO_FINAL_POWER_EPICS':
            nodes['PSL'] = 'FINAL_POWER_EPICS'
        elif nodes['PSL'].arrived and nodes['PSL'] == 'FINAL_POWER_EPICS':
            return True

        #return nodes['PSL'].arrived
        #return True

##############################################
class PSL_TRACKING(GuardState):
    index=100000
    request=True

    def main(self):

        self.timer['fss_check'] = 0		#Set to zero so that it checks immediately
        self.stepping = False		#Initialize stepping as False, becomes true if needed

    def run(self):

        #notify on faults
        if psl_fault():
            return 'PSL_FAULT'
        in_fault()

        if not is_locked():
            log('lock loss, returning to down...')
            return 'DOWN'

        # This section of code sets the FSS gain every ten minutes based on the RefCav Trans Power
        if self.timer['fss_check']:

            log('Checking FSS')

            d_fss_gain = diff_fss_gain()
            self.timer['fss_check'] = 1800  #600	#this is how often the code will check


            if abs(d_fss_gain)>=0.1:
                fss_gStep = np.sign(d_fss_gain)*0.1				#Step Size
                self.fss_nStep = int(round(abs(d_fss_gain/fss_gStep)))	#Number of steps
                fss_string = str(fss_gStep) + ',' + str(self.fss_nStep)	
                log('Number of steps is ' + str(self.fss_nStep))
                log('Step size is ' + str(fss_gStep))


                if not self.fss_nStep == 0:
                    self.fss_step = cdsutils.Step(ezca,'PSL-FSS_COMMON_GAIN', fss_string, time_step=0.2)
                    self.stepping = True
                    self.fss_nStep = 0	# Set back to zero, so as not to overwrite fss_step on next iteration


        if self.stepping:
            self.stepping = not self.fss_step.step()
            return True #This needs to return True, otherwise it will kick IFO out of observing

        return True
        


##################################################

edges = [
    ('FAULT','DOWN'),
    ('DOWN', 'ACQUIRE'),
    ('DOWN', 'GOTO_UNLOCKED_0W'),
    ('GOTO_UNLOCKED_0W','UNLOCKED_0W'),
    ('UNLOCKED_0W','DOWN'),
    #('ACQUIRE', 'LOCK_CONFIRM'),
    ('ACQUIRE', 'ENGAGE_MC2'),
    ('ENGAGE_MC2', 'LOCK_CONFIRM'),
    ('LOCK_CONFIRM', 'LOCK_UP'),
    ('LOCK_UP', 'LOCK_UP2'),
    ('LOCK_UP2', 'LOCK_UP3'),
    ('LOCK_UP3','LOCKED'),
    ('LOCKED', 'GOTO_LOCKED_1W'),
    ('GOTO_LOCKED_1W','LOCKED_1W'),
    ('GOTO_LOCKED_4W','LOCKED_4W'),
    ('GOTO_LOCKED_10W','LOCKED_10W'),
    ('GOTO_LOCKED_20W','LOCKED_20W'),
    ('GOTO_LOCKED_25W','LOCKED_25W'),
    ('LOCKED_1W','GOTO_LOCKED_4W'),
    ('LOCKED_4W','GOTO_LOCKED_10W'),
    ('LOCKED_10W','GOTO_LOCKED_20W'),
    ('LOCKED_10W','GOTO_LOCKED_25W'),
    ('LOCKED_20W','GOTO_LOCKED_25W'),
    ('LOCKED_25W','GOTO_LOCKED_45W'),
    ('LOCKED_25W','GOTO_LOCKED_20W'),
    ('LOCKED_25W','GOTO_LOCKED_10W'),
    ('LOCKED_20W','GOTO_LOCKED_10W'),
    ('LOCKED_4W','GOTO_LOCKED_1W'),
    ('LOCKED_10W','GOTO_LOCKED_4W'),
    ('LOCKED_10W','GOTO_LOCKED_1W'),
    ('LOCKED_1W','GOTO_LOCKED_10W'),
    ('LOCKED_1W','IFO_POWER'),
    #('GOTO_IFO_POWER','IFO_POWER'),
    ('IFO_POWER','IFO_POWER'),
    ('LOCKED_4W','IFO_POWER'),
    ('LOCKED_10W','IFO_POWER'),
    ('LOCKED_20W','IFO_POWER'),
    ('LOCKED_25W','IFO_POWER'),
    ('IFO_POWER','GOTO_LOCKED_1W'),
    ('IFO_POWER','GOTO_LOCKED_4W'),
    ('IFO_POWER','GOTO_LOCKED_10W'),
    ('IFO_POWER','GOTO_LOCKED_20W'),
    ('IFO_POWER','GOTO_LOCKED_25W'),
    ('IFO_POWER','FINAL_IFO_POWER'),
    ('LOCKED_1W','FINAL_IFO_POWER'),
    ('LOCKED_4W','FINAL_IFO_POWER'),
    ('LOCKED_10W','FINAL_IFO_POWER'),
    ('LOCKED_20W','FINAL_IFO_POWER'),
    ('LOCKED_25W','FINAL_IFO_POWER'),
    ('LOCKED_45W','FINAL_IFO_POWER'),
    ('IFO_POWER','FINAL_IFO_POWER'),
    ('FINAL_IFO_POWER','GOTO_LOCKED_1W'),
    ('FINAL_IFO_POWER','GOTO_LOCKED_4W'),
    ('FINAL_IFO_POWER','GOTO_LOCKED_10W'),
    ('FINAL_IFO_POWER','GOTO_LOCKED_20W'),
    ('FINAL_IFO_POWER','GOTO_LOCKED_25W'),
    ('FINAL_IFO_POWER','FINAL_IFO_POWER'),
    ('FINAL_IFO_POWER','IFO_POWER'),
    ('FINAL_IFO_POWER','PSL_TRACKING'),
    ('IFO_POWER','PSL_TRACKING'),
    ('PSL_TRACKING','IFO_POWER')
    ]


edges += [('DOWN','DOWN')]
##################################################
# SVN $Id: IMC_LOCK.py 28550 2024-08-12 22:41:17Z stuart.aston@LIGO.ORG $
# $HeadURL: https://redoubt.ligo-wa.caltech.edu/svn/cds_user_apps/trunk/ioo/l1/guardian/IMC_LOCK.py $
##################################################
